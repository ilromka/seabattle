<?php

class Helper
{
    private static function getConfig(){
        return require ROOT_DIR . '/config/init.php';
    }

    public static function fillField(){
        $config = self::getConfig();
        $field = [];
        for($y = 0; $y < $config['dimensions']['y']; $y++){
            $row = [];
            for($x = 0; $x < $config['dimensions']['x']; $x++){
                array_push($row, Play::STATUSES['empty']);
            }
            array_push($field, $row);
        }
        return $field;
    }

    public static function setShips(){
        $config = self::getConfig();
        $testField = self::fillField();
        $ships = [];
        foreach($config['ships'] as $ship) {
            for($i = 0; $i < $ship['count']; $i++){
                $actualShip = [
                    'structure' => self::getShip($ship['structure']),
                    'x' => 0,
                    'y' => 0,
                    'xSize' => 0,
                    'ySize' => 0
                ];
	            $actualShip['xSize'] = count($actualShip['structure'][0]);
	            $actualShip['ySize'] = count($actualShip['structure']);
	            $positionCorrect = false;
                while(!$positionCorrect){
                    $actualShip['x'] = rand(0, ($config['dimensions']['x'] - 1) - ($actualShip['xSize'] - 1));
                    $actualShip['y'] = rand(0, ($config['dimensions']['y'] - 1) - ($actualShip['ySize'] - 1));
                    $positionCorrect = self::isPositionCorrect($testField, $actualShip);
                }
                array_push($ships, $actualShip);
                $testField = self::fillShip($testField, $actualShip);
            }
        }

        return $ships;
    }

	private static function fillShip($testField, $actualShip){
		for($y = 0; $y < $actualShip['ySize']; $y++){
			for($x = 0; $x < $actualShip['xSize']; $x++){
				if($actualShip['structure'][$y][$x] === Play::STATUSES['past']){
					$testField[$actualShip['y'] + $y][$actualShip['x'] + $x] = Play::STATUSES['past'];
				}
			}
		}
		return $testField;
	}

    public static function shot($game, $y, $x){
        $result = self::isGot($game, $y, $x);
    	if($result['success']){
		    $game['field'][$y][$x] = 2;
            $game['ships'][$result['shipIndex']]['structure'][$y - $game['ships'][$result['shipIndex']]['y']][$x - $game['ships'][$result['shipIndex']]['x']] = Play::STATUSES['got'];
	    }else{
		    $game['field'][$y][$x] = 1;
	    }
    	return $game;
    }

    private static function isGot($game, $yShot, $xShot){
        $result = false;
        $index = null;
    	foreach($game['ships'] as $i => $ship){
    	    for($y = 0; $y < $ship['ySize']; $y++){
                for($x = 0; $x < $ship['xSize']; $x++){
                    if(
                        $yShot >= $ship['y'] &&
                        $yShot <= ($ship['y'] + ($ship['ySize'] - 1)) &&
                        $xShot >= $ship['x'] &&
                        $xShot <= ($ship['x'] + ($ship['xSize'] - 1))
                    ){
                        if($ship['structure'][$yShot - $ship['y']][$xShot - $ship['x']]){
                            $result = true;
                            $index = $i;
                        }
                    }
                }
            }
        }
        return [
            'success' => $result,
            'shipIndex' => $index
        ];
    }

    public static function destroyedShips($game){
        $destroyedShips = [];
        foreach($game['ships'] as $ship){
            $isDestroyed = true;
            for($y = 0; $y < $ship['ySize']; $y++){
                for ($x = 0; $x < $ship['xSize']; $x++){
                    if($ship['structure'][$y][$x] == Play::STATUSES['past']){
                        $isDestroyed = false;
                    }
                }
            }
            if($isDestroyed){
                array_push($destroyedShips, $ship);
            }
        }
        return $destroyedShips;
    }

    public static function gameOver($game){
        $result = false;
        $allShipsCount = 0;
        foreach(self::getConfig()['ships'] as $ship){
            $allShipsCount += $ship['count'];
        }
        if(count(self::destroyedShips($game)) === $allShipsCount){
            $result = true;
        }
        return $result;
    }

    private static function isPositionCorrect($testField, $actualShip){
	    $config = self::getConfig();
	    $result = true;

	    $yStart = $actualShip['y'] - 1;
	    if($yStart < 1){
		    $yStart = 0;
	    }

	    $yEnd = $actualShip['y'] + $actualShip['ySize'];
	    if($yEnd > ($config['dimensions']['y'] - 1)){
		    $yEnd = $config['dimensions']['y'] - 1;
	    }

	    $xStart = $actualShip['x'] - 1;
	    if($xStart < 1){
		    $xStart = 0;
	    }

	    $xEnd = $actualShip['x'] + $actualShip['xSize'];
	    if($xEnd > ($config['dimensions']['x'] - 1)){
	    	$xEnd = $config['dimensions']['x'] - 1;
	    }

	    for($y = $yStart; $y <= $yEnd; $y++){
		    for($x = $xStart; $x <= $xEnd; $x++){
			    if($testField[$y][$x] === Play::STATUSES['past']){
				    $result = false;
			    }
		    }
	    }
	    return $result;
    }

    private static function getShip($prototype){
    	for($i = 0; $i < rand(0, 3); $i++){
    		$newArray = [];
    		for($x = 0; $x < count($prototype[0]); $x++){
			    $row = [];
			    for($y = count($prototype) - 1; $y >= 0; $y--){
				    array_push($row, $prototype[$y][$x]);
			    }
			    array_push($newArray, $row);
		    }
		    $prototype = $newArray;
	    }
        return $prototype;
    }
}