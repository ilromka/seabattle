<?php

class View
{
    protected $vars = [];

    public function render($template){
        include ROOT_DIR . '/templates/' . $template;
    }

    public function __set($name, $value){
        $this->vars[$name] = $value;
    }

    public function __get($name){
        return $this->vars[$name];
    }
}