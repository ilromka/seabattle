<?php

class Play
{
    const STATUSES = [
        'empty' => 0,
        'past' => 1,
        'got' => 2
    ];

    private $vars;

    public function __construct()
    {
        if(!isset($_SESSION['game'])){
            self::init();
        }

        if($_SERVER['REQUEST_METHOD'] === 'POST'){
            switch($_POST['action']){
                case 'shot':
                    $_SESSION['game'] = Helper::shot($_SESSION['game'], $_POST['y'], $_POST['x']);
                    $ships = Helper::destroyedShips($_SESSION['game']);
	                echo json_encode([
	                    'field' => $_SESSION['game']['field'],
                        'ships' => $ships,
                        'gameover' => Helper::gameOver($_SESSION['game'])
                    ]);
                    exit;
	            case 'reset':
		            self::init();
		            break;
                case 'get-data':
                default:
                    echo json_encode([
                        'field' => $_SESSION['game']['field'],
                        'ships' => Helper::destroyedShips($_SESSION['game']),
                        'gameover' => Helper::gameOver($_SESSION['game'])
                    ]);
                    exit;
            }
        }
    }

    private function init(){
	    $field = Helper::fillField();
	    $ships = Helper::setShips();
	    $_SESSION['game'] = [
		    'field' => $field,
		    'ships' => $ships
	    ];
	    $this->vars = [
		    //'field' => $field
	    ];
    }

    public function getVars(){
        return $this->vars;
    }

    public static function getStatuses(){
        return self::STATUSES;
    }
}