export default class Play
{

	constructor(){
		this.field = $('.field');
		$(document).ready(() => {

            this.getData();

            $('.reset').on('click', event => {
            	event.preventDefault();
	            $.ajax({
		            type: 'POST',
		            url: '/',
		            data: {
			            action: 'reset'
		            },
		            success: () => {
                        $('.gameover').hide();
			            this.getData();
		            }
	            });
            });

		});
	}

	getData(){
		$.ajax({
			type: 'POST',
			url: '/',
			data: {
				action: 'get-data'
			},
			success: data => {
				data = JSON.parse(data);
				this.fillField(data.field);
				Play.redrawField(data.field);
				if(data.ships.length){
					data.ships.forEach(ship => {
						Play.fillDestroyed(ship);
					});
				}
                if(data.gameover){
                    $('.gameover').show();
                }
			}
		});
	}

	fillField(data){
        $('.cell').unbind();
        this.field.html('');
        for(let y = 0; y < data.length; y++) {
            for(let x = 0; x < data[0].length; x++){
            	this.field.append('<div class="cell" data-y="' + y + '" data-x="' + x + '"></div>');
			}
        }
        $('.cell').on('click', event => {
            let target = $(event.target);
            event.preventDefault();
            let y = target.attr('data-y');
            let x = target.attr('data-x');
            this.shot(y, x);
        });
	}

	shot(y, x){
        $.ajax({
            type: 'POST',
            url: '/',
            data: {
                action: 'shot',
				y: y,
				x: x
            },
            success: data => {
                data = JSON.parse(data);
				Play.redrawField(data.field);
				if(data.ships.length){
					data.ships.forEach(ship => {
						Play.fillDestroyed(ship);
					});
				}
                if(data.gameover){
                    $('.gameover').show();
                }
            }
        });
	}

	static fillDestroyed(ship){
		for(let y = 0; y < ship.ySize; y++){
            for(let x = 0; x < ship.xSize; x++){
            	if(ship.structure[y][x]){
                    let $cell = $('[data-y=' + (y + ship.y) + '][data-x=' + (x + ship.x) + ']');
                    if(ship.structure[y - 1] === undefined || !ship.structure[y - 1][x]){
                        $cell.addClass('destroyed-top');
					}
                    if(ship.structure[y][x + 1] === undefined || !ship.structure[y][x + 1]){
                        $cell.addClass('destroyed-right');
                    }
                    if(ship.structure[y + 1] === undefined || !ship.structure[y + 1][x]){
                        $cell.addClass('destroyed-bottom');
                    }
                    if(ship.structure[y][x - 1] === undefined || !ship.structure[y][x - 1]){
                        $cell.addClass('destroyed-left');
                    }
				}
            }
		}
	}

	static redrawField(data){
		for(let y = 0; y < data.length; y++){
			for(let x = 0; x < data[0].length; x++){
				let $cell = $('[data-y=' + y + '][data-x=' + x + ']');
				switch(data[y][x]){
					case 1:
						$cell.addClass('past');
						break;
					case 2:
						$cell.addClass('got');
						break;
				}
			}
		}
	}

	static gameOver(){
		//
	}

}