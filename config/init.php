<?php
return [
    'dimensions' => [
        'x' => 10,
        'y' => 10
    ],
    'ships' => [
        [
            'structure' => [
                [0, 0, 1],
                [1, 1, 1]
            ],
            'count' => 1
        ],
        [
            'structure' => [
                [1, 1, 1, 1]
            ],
            'count' => 1
        ],
        [
            'structure' => [
                [1]
            ],
            'count' => 2
        ]
    ]
];