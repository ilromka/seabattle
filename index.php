<?php
session_start();

define("ROOT_DIR", __DIR__);

function __autoload($className){
  $className = str_replace("..", "", $className);
  require_once( "classes/$className.php" );
}

$play = new Play();

$view = new View();
$view->vars = $play->getVars();
$view->render('template.phtml');